import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/task2a', (req, res) => {
  const summ = ( parseInt(req.query.a, 10) || 0 ) + ( parseInt(req.query.b) || 0 );
  console.log(summ);
  res.send(''+summ+'');
});
app.get('/task2b', (req, res) => {
  console.log("*****************************************");
  console.log(req.query.fullname);
  const fullname = req.query.fullname.trim().replace(/\s+/g, ' ').replace(/[']/g, '') || '';
  console.log(fullname);
  let Fio =  '';

  console.log(/^[a-zA-Zа-яёА-ЯЁäöüÄÖÜßèéûó\s]+$/.test(fullname));
  if( /^[a-zA-Zа-яёА-ЯЁäöüÄÖÜßèéûó\s]+$/.test(fullname) )  {
    const fullnameArray = fullname.split(" ");
    console.log(fullnameArray.length);

    if( fullnameArray.length > 3 ) Fio = 'Invalid fullname';
    else if( fullnameArray.length > 2 ) Fio =  fullnameArray[2].charAt(0).toUpperCase() + fullnameArray[2].substr(1).toLowerCase() + ' ' + fullnameArray[0].charAt(0).toUpperCase() + '. ' + fullnameArray[1].charAt(0).toUpperCase() + '.';
    else if( fullnameArray.length > 1 ) Fio =  fullnameArray[1].charAt(0).toUpperCase() + fullnameArray[1].substr(1).toLowerCase() + ' ' + fullnameArray[0].charAt(0).toUpperCase() + '.';
    else Fio =  fullnameArray[0].charAt(0).toUpperCase() + fullnameArray[0].substr(1).toLowerCase();
  }
  else {
    Fio = 'Invalid fullname';
  }
  res.send(Fio);
});
app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
